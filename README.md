AWS Utils
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Various utilities to be used in other roles.

* Verify AWS caller identity belongs to target AWS account
* Get latest AWS AMI's (copies and encrypts AMI if requested)
* Compile and upload Lambda function with dependencies to S3
* Get Cloudformation Changeset description for specific or all stacks

When `lambda_delete_source` is set to `True` the .py and .go files will be deleted from the zip file.
This will not impact the ability for the lambda to work due to the .pyc files being uploaded, but might make debugging harder/impossible.
Note that multi-file lambda's would already be a pain to debug through the lambda console before this ;)

Currently supported lambda runtimes:
* Python
* Go

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x
Docker that works under your user.

Required python modules:
* boto
* boto3
* awscli
* docker

#### _Mac_
You need to add a file share to your docker host:
- /var/folders

Dependencies
------------
None

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_id      : <aws account id (number)>
tag_prefix      : <'mcf' by default, will be used in various tags as prefix>
```
Role Variables
--------------
```yaml
cloudformation_tags: {}
tag_prefix         : "mcf"
aws_utils_params   :
  debug                 : False
  ecs_encrypt_ami       : True
  ec2_encrypt_ami       : True
  ec2v2_encrypt_ami     : "{{ aws_utils_params.ec2_encrypt_ami }}"
  kms_key_arn           : ""
  lambda_delete_source  : True
```
Example Playbooks
----------------
### Package Python lambda
```yaml
---
# Package python lambda functions and upload to S3
- include_role:
    name: aws-utils
    tasks_from: lambda-python-package.yml
  vars:
    lambda_bucket_name: "{{ aws_lambda_image_converter_params.lambda_bucket_name }}"
    lambda_function: "{{ aws_lambda_image_converter_params.lambda }}"
```

### Package Go lambda
```yaml
---
# Package go lambda functions and upload to S3
- include_role:
    name: aws-utils
    tasks_from: lambda-go-package.yml
  vars:
    lambda_bucket_name: "{{ aws_lambda_custom_style_writer_params.lambda_bucket_name }}"
    lambda_function: "{{ aws_lambda_custom_style_writer_params.lambda }}"
```

### Create Layer
```
# Package lambda layer and upload to S3
- include_role:
    name: aws-utils
    tasks_from: lambda-layer-package.yml
  vars:
    lambda_bucket_name: "{{ aws_lambda_custom_style_writer_params.lambda_bucket_name }}"
    layer: "{{ aws_lambda_custom_style_writer_params.layer }}"

```

### Get latest AMI's
This will fetch the latest AMI's through AWS provided SSM parameters.
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    tag_prefix      : "mcf"
    aws_utils_params:
      ecs_encrypt_ami  : False
      ec2_encrypt_ami  : True
      kms_key_arn      : ""

  tasks:
    - name: Verify AWS Profile
      include_role:
        name: aws-utils
        tasks_from: verify-aws-identity
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
```
First verifies that your calling AWS account matches the destination.
Next it will fetch the latest AMI's through AWS provided SSM parameters.
When encrypt is enabled the AMI will be copied and encrypted, tagged with a copysource.
Returns a dictionary with request AMI's, looks like this:

```yaml
amis:
  ec2_latest_ami: ami-12345678901234567
  ecs_latest_ami: ami-09876543211234566
```
These will be the copied (encrypted) AMI's when requested.

```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    tag_prefix      : "mcf"
    aws_utils_params:
      ec2_encrypt_ami: True
      ecs_encrypt_ami: False
      kms_key_arn    : ""
  
  tasks:
    - name: "Show changes for s3 buckets"
      include_role:
        name: aws-utils
        tasks_from: describe-changesets
      vars:
        stacks: s3
          # - lambda-s3
          # - setup-s3
```

Above snippet will fetch all cloudformation changesets and display the ones matching 's3' anywhere, or when stacks is passed as an array it will return the matching entries.

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
