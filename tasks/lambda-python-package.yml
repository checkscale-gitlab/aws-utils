---
#
# We build the lambda through a docker now.
# Steps:
# 1. Build our lambda-builder docker image
# 2. Put lambda files in tempdir, add dependencies.txt
# 3. Run docker
# 4. Put output in S3
#

#######################################################################
#                                                                     #
#   Upload required lambda functions into S3.                         #
#                                                                     #
#######################################################################
#
# Check if we already have the lambda
#

- name: "debug lambda function"
  debug: var=lambda_function

- name: "sha256 | generate"
  stat:
    path: "{{ file }}"
    get_checksum: True
    checksum_algorithm: sha256
  register: stat_output
  with_items: "{{ lambda_function.files | sort }}"
  loop_control:
    loop_var: file
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "statsum | clear"
  set_fact:
    statsum: {}

- name: "sha256 | grab checksums"
  set_fact:
    statsum: "{{ statsum | default({}) | combine( { (stat.file| replace(lambda_role_path, '') | replace(role_path, '')): stat.stat.checksum }) }}"
  with_items: "{{ stat_output.results }}"
  loop_control:
    loop_var: stat
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "sha256 | add dependencies to checksums"
  set_fact:
    statsum: "{{ statsum | default({}) | combine( { dep: dep }) }}"
  with_items: "{{ lambda_function.dependencies | sort  }}"
  loop_control:
    loop_var: dep
  when: lambda_function.dependencies is defined
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "sha256 | combine"
  set_fact:
   statsum: "{{ statsum }}"
   shasum: "{{ statsum | hash('sha256') }}"
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "sha256 | debug"
  debug: var=shasum
  when: aws_utils_params.debug
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "fact | s3key | set"
  set_fact:
    lambda_s3key: "{{ lambda_s3key | default({}) | combine( { lambda_function.name: account_abbr + '-' + lambda_function.name + '-' + shasum + '.zip' } ) }}"
  tags:
    - aws
    - lambda
    - lambda:prepare

# See if this has changed - if the file is already in s3 this is a noop
- name: "s3key | check s3 contents"
  aws_s3:
    region: "{{ aws_region }}"
    bucket: "{{ lambda_bucket_name }}"
    mode: list
    prefix: "lambda/{{ lambda_s3key[lambda_function.name] }}"
    max_keys: 1
  register: keys
  ignore_errors: True
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "s3key | check changed"
  set_fact:
    createzip: "{{ False if ((keys is succeeded) and (keys.s3_keys|length == 1)) else True }}"
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "s3key | debug | createzip"
  debug:
    msg: "Createzip is {{ createzip }} for {{ lambda_function.name }}"
  when: aws_utils_params.debug
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# In case we need to recreate the bundle, start with a tempdir in which we store the lambda files.
#
- name: "tempdir | create"
  tempfile:
    state: directory
    suffix: lambda
  register: tempdir
  when: createzip
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# Make sure we have our lambda builder
#
- name: "docker | create"
  docker_image:
     name  : "mcf-lambda-builder-python"
     tag   : "latest"
     source: "build"
     build :
       pull: True
       path: "{{ role_path }}/files/docker/lambda-builder-python"
  register: mcf-lambda-builder-python
  when: createzip
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# Install the lambda function's files in the tempdir
#
- name: "copy | lambdascript"
  copy:
    dest: "{{ tempdir.path + '/' + (file | basename) }}"
    src: "{{ file }}"
  with_items: "{{ lambda_function.files }}"
  loop_control:
    loop_var: file
  when: createzip
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# Make sure we have an index.py
#
- name: "copy | lambdascript | indexhandler"
  copy:
    dest: "{{ tempdir.path + '/index' + ((file | splitext)[1] | default('.py')) }}"
    src: "{{ file }}"
  with_items: "{{ lambda_function.files.0 }}"
  loop_control:
    loop_var: file
  when: createzip
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# Install dependencies through virtualenv's pip
#
- name: "Lambda Prepare | create dependencies file"
  file:
    path: "{{ tempdir.path + '/dependencies.txt' }}"
    state: touch
  when:
    - createzip
    - lambda_function.dependencies is defined
  tags:
    - aws
    - lambda
    - lambda:prepare

- name: "Lambda Prepare | create dependencies file"
  blockinfile:
    path: "{{ tempdir.path + '/dependencies.txt' }}"
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ dependency }}"
    block: |
      {{ dependency }}
  with_items: "{{ lambda_function.dependencies }}"
  loop_control:
    loop_var: dependency
  when:
    - createzip
    - lambda_function.dependencies is defined
  tags:
    - aws
    - lambda
    - lambda:prepare

#
# If we are running in docker (i.e. AWX) we can't mount our temp to /lambda inside another docker
# In that case upload our source to S3 and pass it to docker run as environment variable
#
- name: CreateZIP block
  block:
    - name: "Lambda Prepare | check if we are running in docker"
      stat:
        path: "/.dockerenv"
      register: isdocker

    - name: "Lambda Prepare | build source bundle due to dockerception"
      command: "zip -r {{ tempdir.path }}/lambda-src.zip ."
      args:
        chdir: "{{ tempdir.path }}"
        creates: "{{ tempdir.path }}/lambda-src.zip"
      when:
        - isdocker.stat.exists

    - name: "Lambda Prepare | put source bundle in s3"
      aws_s3:
        region: "{{ aws_region }}"
        bucket: "{{ lambda_bucket_name }}"
        object: "lambda/{{ lambda_s3key[lambda_function.name] }}-src"
        src   : "{{ tempdir.path + '/lambda-src.zip' }}"
        mode  : put
        overwrite: different
      register: s3_src
      when:
        - isdocker.stat.exists

    - name: "Lambda Prepare | get source bundle s3 url"
      aws_s3:
        region: "{{ aws_region }}"
        bucket: "{{ lambda_bucket_name }}"
        object: "lambda/{{ lambda_s3key[lambda_function.name] }}-src"
        mode  : geturl
      register: s3_src_url
      when:
        - isdocker.stat.exists

    #
    # Run docker to create lambda.zip
    #
    - name: "Lambda Package | create lambda.zip through docker"
      docker_container:
        name: lambdabuilder
        image: mcf-lambda-builder-python:latest
        #auto_remove: yes
        #cleanup: yes
        detach: no
        env:
          # Set to 1 to get rid of .py files
          DELETESOURCE: "{{ (aws_utils_params.lambda_delete_source | default(False)) | int | ternary('1', '0') }}"
          S3_SOURCE: "{{ s3_src_url.url if 'url' in s3_src_url else omit }}"
          S3_TARGET: "{{ lookup('aws_s3_presigned', lambda_bucket_name, key='lambda/'+lambda_s3key[lambda_function.name], region=aws_region) if isdocker.stat.exists else omit }}"
        ignore_image: True
        recreate: True
        volumes:
          - "{{ (tempdir.path + ':/lambda') if not isdocker.stat.exists else omit }}"
      register: mcf-build

    - name: "s3key | fact | debug"
      debug: var=lambda_s3key
      when: aws_utils_params.debug

    - name: "zip | s3 | upload"
      aws_s3:
        region: "{{ aws_region }}"
        bucket: "{{ lambda_bucket_name }}"
        object: "lambda/{{ lambda_s3key[lambda_function.name] }}"
        src   : "{{ tempdir.path + '/lambda.zip' }}"
        mode  : put
        overwrite: different
      when: not isdocker.stat.exists

    ####################################################################################
    #
    # cleanup
    #
    ####################################################################################
    - name: "tempfile | delete"
      file:
        name: "{{ tempdir.path }}"
        state: absent
      tags:
        - lambda:cleanup

  when:
    - createzip
  tags:
    - aws
    - lambda
    - lambda:prepare
