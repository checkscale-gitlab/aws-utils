Go Lambda builder
========================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

The container creates a zip file to be uploaded to S3 for AWS Lambda, which is handled in the lambda-go-package task.
First the task copies the go source files to MYTEMPDIR.
It runs the docker like this:
```bash
docker run --rm -u "${EUID}" -v ${MYTEMPDIR}:/lambda mcf-lambda-builder-go:latest
```
After it's done there should be a `lambda.zip` file in $MYTEMPDIR

Requirements
------------
Docker that works under your user.

Dependencies
------------
None

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
